/**
 * Created by Ser on 22/06/2014.
 */
angular.module('incidenciasDefApp').factory('Comunidad',['$resource',function($resource){
    var baseUrl = 'http://127.0.0.1:9000/IncidenciasDef/API';

    return $resource(baseUrl+'/comunidad',null,{
        'getAll':{method:'GET', isArray: true},
        'get': {url:baseUrl +'/comunidad/:id',method:'GET', isArray: false},
        'update':{url:baseUrl +'/comunidad/:id',method:'PUT'},
        'create': {url:baseUrl +'/comunidad/create',method:'POST'},
        'delete':{url:baseUrl +'/comunidad/delete/:id',method:'DELETE'}
    });

}]);

angular.module('incidenciasDefApp').factory('Entrega',['$resource',function($resource){
    var baseUrl = 'http://127.0.0.1:9000/IncidenciasDef/API';

    return $resource(baseUrl+'/entrega',null,{
        'getAll':{method:'GET', isArray: true},
        'get': {url:baseUrl +'/entrega/:id',method:'GET', isArray: false},
        'between': {url:baseUrl +'/entrega/between/:inicio/:final',method:'GET', isArray: true},

        'update':{url:baseUrl +'/entrega/:id',method:'PUT'},
        'create': {url:baseUrl +'/entrega/create',method:'POST'},
        'delete':{url:baseUrl +'/entrega/delete/:id',method:'DELETE'}
    });

}]);

angular.module('incidenciasDefApp').factory('Tipo',['$resource',function($resource){
    var baseUrl = 'http://127.0.0.1:9000/IncidenciasDef/API';

    return $resource(baseUrl+'/tipo',null,{
        'getAll':{method:'GET', isArray: true},
        'get': {url:baseUrl +'/tipo/:id',method:'GET', isArray: false},
        'update':{url:baseUrl +'/tipo/:id',method:'PUT'},
        'create': {url:baseUrl +'/tipo/create',method:'POST'},
        'delete':{url:baseUrl +'/tipo/delete/:id',method:'DELETE'}
    });

}]);


angular.module('incidenciasDefApp').factory('Contabilidad',['$resource',function($resource){
    var baseUrl = 'http://127.0.0.1:9000/IncidenciasDef/API';

    return $resource(baseUrl+'/contabilidad',null,{
        'getAll':{method:'GET', isArray: true},
        'get': {url:baseUrl +'/contabilidad/:id',method:'GET', isArray: false},
        'between': {url:baseUrl +'/contabilidad/between/:inicio/:final',method:'GET', isArray: true},
        'marcar': {url:baseUrl +'/contabilidad/marcar/:id',method:'GET'},
        'update':{url:baseUrl +'/contabilidad/:id',method:'PUT'},
        'create': {url:baseUrl +'/contabilidad/create',method:'POST'},
        'delete':{url:baseUrl +'/contabilidad/delete/:id',method:'DELETE'}
    });

}]);

angular.module('incidenciasDefApp').factory('Caja',['$resource',function($resource){
    var baseUrl = 'http://127.0.0.1:9000/IncidenciasDef/API';

    return $resource(baseUrl+'/caja',null,{
        'getAll':{method:'GET', isArray: true},
        'get': {url:baseUrl +'/caja/:id',method:'GET', isArray: false},
        'between': {url:baseUrl +'/caja/between/:inicio/:final',method:'GET', isArray: true},
        'marcar': {url:baseUrl +'/caja/marcar/:id',method:'GET'},
        'update':{url:baseUrl +'/caja/:id',method:'PUT'},
        'create': {url:baseUrl +'/caja/create',method:'POST'},
        'delete':{url:baseUrl +'/caja/delete/:id',method:'DELETE'}
    });

}]);
angular.module('incidenciasDefApp').factory('Movimiento',['$resource',function($resource){
    var baseUrl = 'http://127.0.0.1:9000/IncidenciasDef/API';

    return $resource(baseUrl+'/movimiento',null,{
        'getAll':{url:baseUrl +'/movimiento/:id',method:'GET', isArray: true},
        'get': {url:baseUrl +'/movimiento/get/:id',method:'GET', isArray: false},
        'between': {url:baseUrl +'/movimiento/between/:inicio/:final',method:'GET', isArray: true},
        'marcar': {url:baseUrl +'/movimiento/marcar/:id',method:'GET'},
        'update':{url:baseUrl +'/movimiento/:id',method:'PUT'},
        'create': {url:baseUrl +'/movimiento/create',method:'POST'},
        'delete':{url:baseUrl +'/movimiento/delete/:id',method:'DELETE'}
    });

}]);

angular.module('incidenciasDefApp').factory('Persona',['$resource',function($resource){
    var baseUrl = 'http://127.0.0.1:9000/IncidenciasDef/API';

    return $resource(baseUrl+'/persona',null,{
        'getAll':{method:'GET', isArray: true},
        'getWithPhones': {url:baseUrl +'/persona/phones',method:'GET', isArray: true},
        'getID': {url:baseUrl +'/persona/:id',method:'GET', isArray: false},
        'between': {url:baseUrl +'/persona/between/:inicio/:final',method:'GET', isArray: true},
        'marcar': {url:baseUrl +'/persona/marcar/:id',method:'GET'},
        'update':{url:baseUrl +'/persona/:id',method:'PUT'},
        'create': {url:baseUrl +'/persona/create',method:'POST'},
        'delete':{url:baseUrl +'/persona/delete/:id',method:'DELETE'}
    });

}]);

angular.module('incidenciasDefApp').factory('Telefono',['$resource',function($resource){
    var baseUrl = 'http://127.0.0.1:9000/IncidenciasDef/API';

    return $resource(baseUrl+'/telefono',null,{
        'getAll':{method:'GET', isArray: true},
        'getWithPhones': {url:baseUrl +'/telefono/phones',method:'GET', isArray: true},
        'get': {url:baseUrl +'/telefono/:id',method:'GET', isArray: false},
        'between': {url:baseUrl +'/telefono/between/:inicio/:final',method:'GET', isArray: true},
        'marcar': {url:baseUrl +'/telefono/marcar/:id',method:'GET'},
        'update':{url:baseUrl +'/telefono/:id',method:'PUT'},
        'create': {url:baseUrl +'/telefono/create',method:'POST'},
        'delete':{url:baseUrl +'/telefono/delete/:id',method:'DELETE'},
        'checkPhonePersona':{url:baseUrl +'/telefono/checkPersona/:phone/:id',method:'GET', isArray: false},
        'checkPhoneEmpresa':{url:baseUrl +'/telefono/checkEmpresa/:phone/:id',method:'GET', isArray: false},
        'checkPhoneIncidencia':{url:baseUrl +'/telefono/checkIncidencia/:phone/:id',method:'GET', isArray: false}
    });

}]);

angular.module('incidenciasDefApp').factory('Empresa',['$resource',function($resource){
    var baseUrl = 'http://127.0.0.1:9000/IncidenciasDef/API';

    return $resource(baseUrl+'/empresa',null,{
        'getAll':{method:'GET', isArray: true},
        'getWithPhones': {url:baseUrl +'/empresa/phones',method:'GET', isArray: true},
        'get': {url:baseUrl +'/empresa/:id',method:'GET', isArray: false},
        'getID': {url:baseUrl +'/empresa/:id',method:'GET', isArray: false},
        'between': {url:baseUrl +'/empresa/between/:inicio/:final',method:'GET', isArray: true},
        'marcar': {url:baseUrl +'/empresa/marcar/:id',method:'GET'},
        'update':{url:baseUrl +'/empresa/:id',method:'PUT'},
        'create': {url:baseUrl +'/empresa/create',method:'POST'},
        'delete':{url:baseUrl +'/empresa/delete/:id',method:'DELETE'}
    });

}]);

angular.module('incidenciasDefApp').factory('Incidencia',['$resource',function($resource){
    var baseUrl = 'http://127.0.0.1:9000/IncidenciasDef/API';

    return $resource(baseUrl+'/incidencia',null,{
        'getAll':{url:baseUrl +'/incidencia/phone',method:'GET', isArray: true},
        'getWithPhones': {url:baseUrl +'/incidencia/phones',method:'GET', isArray: true},
        'getTrash': {url:baseUrl +'/incidencia/phone/trash',method:'GET', isArray: true},
        'get': {url:baseUrl +'/incidencia/:id',method:'GET', isArray: false},
        'getID': {url:baseUrl +'/incidencia/:id',method:'GET', isArray: false},
        'between': {url:baseUrl +'/incidencia/between/:inicio/:final',method:'GET', isArray: true},
        'marcar': {url:baseUrl +'/incidencia/marcar/:id',method:'GET'},
        'resolve': {url:baseUrl +'/incidencia/resolve/:id',method:'GET'},
        'update':{url:baseUrl +'/incidencia/:id',method:'PUT'},
        'create': {url:baseUrl +'/incidencia/create',method:'POST'},
        'delete':{url:baseUrl +'/incidencia/delete/:id',method:'GET'},
        'restore':{url:baseUrl +'/incidencia/restore/:id',method:'GET'},
        'purgue':{url:baseUrl +'/incidencia/purgue/:id',method:'DELETE'}
    });

}]);
angular.module('incidenciasDefApp').factory('Seguimiento',['$resource',function($resource){
    var baseUrl = 'http://127.0.0.1:9000/IncidenciasDef/API';

    return $resource(baseUrl+'/seguimiento',null,{
        'getAll':{url:baseUrl +'/seguimiento/incidencia/:id',method:'GET', isArray: true},
        'getWithPhones': {url:baseUrl +'/seguimiento/phones',method:'GET', isArray: true},
        'get': {url:baseUrl +'/seguimiento/:id',method:'GET', isArray: false},
        'getID': {url:baseUrl +'/seguimiento/:id',method:'GET', isArray: false},
        'between': {url:baseUrl +'/seguimiento/between/:inicio/:final',method:'GET', isArray: true},
        'marcar': {url:baseUrl +'/seguimiento/marcar/:id',method:'GET'},
        'update':{url:baseUrl +'/seguimiento/:id',method:'PUT'},
        'create': {url:baseUrl +'/seguimiento/create',method:'POST'},
        'delete':{url:baseUrl +'/seguimiento/delete/:id',method:'DELETE'}
    });

}]);

angular.module('incidenciasDefApp').factory('Tarea',['$resource',function($resource){
    var baseUrl = 'http://127.0.0.1:9000/IncidenciasDef/API';

    return $resource(baseUrl+'/tarea',null,{
        'getAll':{method:'GET', isArray: true},
        'getWithPhones': {url:baseUrl +'/tarea/phones',method:'GET', isArray: true},
        'get': {url:baseUrl +'/tarea/:id',method:'GET', isArray: false},
        'getID': {url:baseUrl +'/tarea/:id',method:'GET', isArray: false},
        'between': {url:baseUrl +'/tarea/between/:inicio/:final',method:'GET', isArray: true},
        'marcar': {url:baseUrl +'/tarea/marcar/:id',method:'GET'},
        'update':{url:baseUrl +'/tarea/:id',method:'PUT'},
        'create': {url:baseUrl +'/tarea/create',method:'POST'},
        'delete':{url:baseUrl +'/tarea/delete/:id',method:'DELETE'},
        'getTrash': {url:baseUrl +'/tarea/view/trash',method:'GET', isArray: true},
        'delete':{url:baseUrl +'/tarea/delete/:id',method:'GET'},
        'resolve': {url:baseUrl +'/tarea/resolve/:id',method:'GET'},
        'restore':{url:baseUrl +'/tarea/restore/:id',method:'GET'},
        'purgue':{url:baseUrl +'/tarea/purgue/:id',method:'DELETE'}
    });

}]);
angular.module('incidenciasDefApp').factory('Aviso',['$resource',function($resource){
    var baseUrl = 'http://127.0.0.1:9000/IncidenciasDef/API';

    return $resource(baseUrl+'/aviso',null,{
        'getAll':{method:'GET', isArray: true},
        'getWithPhones': {url:baseUrl +'/aviso/phones',method:'GET', isArray: true},
        'get': {url:baseUrl +'/aviso/:id',method:'GET', isArray: false},
        'getID': {url:baseUrl +'/aviso/:id',method:'GET', isArray: false},
        'between': {url:baseUrl +'/aviso/between/:inicio/:final',method:'GET', isArray: true},
        'marcar': {url:baseUrl +'/aviso/marcar/:id',method:'GET'},
        'update':{url:baseUrl +'/aviso/:id',method:'PUT'},
        'create': {url:baseUrl +'/aviso/create',method:'POST'},
        'delete':{url:baseUrl +'/aviso/delete/:id',method:'DELETE'}
    });

}]);
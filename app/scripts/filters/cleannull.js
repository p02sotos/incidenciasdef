'use strict';

/**
 * @ngdoc filter
 * @name incidenciasDefApp.filter:cleanNull
 * @function
 * @description
 * # cleanNull
 * Filter in the incidenciasDefApp.
 */
angular.module('incidenciasDefApp')
  .filter('cleanNull', function () {
    return function (obj) {
        var objectNullToEmpty = function (obj) {
            for (var key in obj) {
                if (obj[key] === null) {
                    obj[key] = '';
                }
                if (typeof obj[key] === 'object') {
                    objectNullToEmpty(obj[key]);
                }
            }
            return obj;
        };
        return objectNullToEmpty(obj);
    };
  });

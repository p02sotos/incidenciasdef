'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:CajacreateCtrl
 * @description
 * # CajacreateCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('CajacreateCtrl',['$scope', '$location','Caja', 'Comunidad', function ($scope,$location, Caja, Comunidad) {

        $scope.caja = {
            Texto: '',
            Notas:'',
            ComunidadId:''
        };
        $scope.submit = function(){

            $scope.caja = Caja.create($scope.caja);
            $scope.caja.$promise.then(function(){
                $location.path('/caja');
            });
        };


        $scope.comunidades = Comunidad.getAll();
        $scope.comunidades.$promise.then(function(result){
            $scope.comunidades = result;
        });
        $scope.selectComunidad = function(nombre, id){
            $scope.comunidadNombre = nombre;

            $scope.caja['ComunidadId'] = id;
        };



    }]);

'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:EntregaCtrl
 * @description
 * # EntregaCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('EntregaCtrl',['$scope','$location' ,'$routeParams','Entrega', function ($scope, $location, $routeParams, Entrega) {
        $scope.filtro = '$';
        $scope.deleteId = '';
        $scope.isFilter = true;
        $scope.changeSearch = function(){
            $scope.isCollapsed = !$scope.isCollapsed;
            $scope.isFilter = !$scope.isFilter;
        }
        $scope.search = {FechaEntrega:'', 'Comunidad.Nombre':'', 'Tipo.Tipo':'',Descripcion:'',$:''};
        $scope.entregas = Entrega.getAll();
        $scope.entregas.$promise.then(function (result) {
            $scope.entregas = result;

        });
        $scope.radioModel = 'Todo';
        $scope.isCollapsed= true;

        $scope.edit = function ($id) {
            $location.path('/entrega/view/' + $id);
        };
        $scope.toTipo = function () {
            $location.path('/tipo');
        };
        $scope.remove = function ($id) {
            console.log($id);
            $scope.entregas = Entrega.delete({id: $id});
            $scope.entregas.$promise.then(function () {
                $scope.entregas = Entrega.getAll();
            });
        };
        $scope.calendar = function () {
            alert ('To calendar'); //TODO
        };
        $scope.create = function () {
            $location.path('/entrega/create');
        };
        $scope.changeFilterBy = function(filter){
            $scope.filtro = filter;
            $scope.isCollapsed =true;
            $scope.isFilter = true;
        };
        $scope.getIdToRemove = function(id){
            console.log('Id a borrar: '+id);
            $scope.deleteId = id;
        };

        $scope.buscarBetween = function(){
            var inicio = $scope.inicio;
            var final = $scope.final;
            $scope.entregas = Entrega.between({inicio:inicio,final:final});
            $scope.entregas.$promise.then(function(result){
                $scope.entregas = result;
            })
        }
        $scope.loadData = function(){
            $scope.entregas = Entrega.getAll();
            $scope.entregas.$promise.then(function (result) {
                $scope.entregas = result;


            });
        }
        $scope.createAviso = function(entrega){
            alert("TODO");
        }
        $scope.createTarea = function(entrega){
            alert("TODO");
        }

        $scope.orderFields = [
            "Nombre",
            "'Comunidad.Nombre'",
            "FechaEntrega"

        ]
        $scope.changeOrder = function(){
           if ($scope.reverse){
               $scope.reverse = false;

           }else {
               $scope.reverse = true;
           }

        }


        $scope.tooltips = {
            list: {
                "title": "Crear Tarea a partir de esta entrega",
                "placement": 'right',
                delay: 500
            },
            aviso: {
                "title": "Crear Aviso a partir de esta entrega",
                delay: 500
            },
            editar: {
                "title": "Editar entrega",
                delay: 500
            },
            borrar: {
                "title": "Borrar"
            }
        };
  }]);

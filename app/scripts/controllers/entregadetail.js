'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:EntregadetailCtrl
 * @description
 * # EntregadetailCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('EntregaDetailCtrl',['$scope', '$location','$routeParams','Entrega','Comunidad','Tipo' ,function ($scope,$location,$routeParams,Entrega, Comunidad, Tipo) {

        $scope.entrega = Entrega.get({id:$routeParams.id});
        $scope.entrega.$promise.then(function(result){
            $scope.entrega = result;
            $scope.tipoId = $scope.entrega['Tipo.Id'];
            console.log($scope.tipoId);
            $scope.tipo = Tipo.get({id: $scope.tipoId});
            $scope.tipo.$promise.then(function(result){
                $scope.tipo = result;
                $scope.tipos = Tipo.getAll();
                $scope.tipos.$promise.then(function(result){
                    $scope.tipos = result;
                    $scope.selectedOption = $scope.tipo.Id;
                });
            });
            $scope.comunidadNombre = $scope.entrega['Comunidad.Nombre'];

        });

        $scope.submit= function(){
            $scope.entrega['Tipo.Id']= $scope.selectedOption;
            $scope.ent = Entrega.update({id:$routeParams.id},$scope.entrega);
            $scope.ent.$promise.then(function(){
                $location.path('/entrega');
            });

        };
        $scope.comunidades = Comunidad.getAll();

        $scope.comunidades.$promise.then(function(result){
            $scope.comunidades = result;
        });

        $scope.selectComunidad = function(nombre, id){
            $scope.comunidadNombre = nombre;
            $scope.entrega['Comunidad.Id'] = id;

        };


    }]);
'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:ContabilidadCtrl
 * @description
 * # ContabilidadCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('ContabilidadCtrl',['$scope','$location' ,'$routeParams','Contabilidad', function ($scope, $location, $routeParams, Contabilidad) {
        $scope.filtro = '$';
        $scope.deleteId = '';
        $scope.isFilter = true;
        $scope.changeSearch = function(){
            $scope.isCollapsed = !$scope.isCollapsed;
            $scope.isFilter = !$scope.isFilter;
        }
        $scope.search = {FechaEntrega:'', 'Comunidad.Nombre':'', 'Notas':'',Marcada:'',$:''};
        $scope.contabilidades = Contabilidad.getAll();
        $scope.contabilidades.$promise.then(function (result) {
            $scope.contabilidades = result;

        });
        
         $scope.orderFields = [
            "FechaEntrega",
            "'Comunidad.Nombre'"          

        ]
        $scope.reverse = false;
         $scope.changeOrder = function(){
           if ($scope.reverse){
               $scope.reverse = false;

           }else {
               $scope.reverse = true;
           }

        }

        $scope.radioModel = 'Todo';
        $scope.isCollapsed= true;

        $scope.edit = function ($id) {
            $location.path('/contabilidad/view/' + $id);
        };
        $scope.toTipo = function () {
            $location.path('/tipo');
        };
        $scope.remove = function ($id) {

            $scope.contabilidades = Contabilidad.delete({id: $id});
            $scope.contabilidades.$promise.then(function () {
                $scope.contabilidades = Contabilidad.getAll();
            });
        };
        $scope.calendar = function () {
            alert ('To calendar'); //TODO
        };
        $scope.create = function () {
            $location.path('/contabilidad/create');
        };
        $scope.changeFilterBy = function(filter){
            $scope.filtro = filter;
            $scope.isCollapsed =true;
            $scope.isFilter = true;
        };
        $scope.getIdToRemove = function(id){
            console.log('Id a borrar: '+id);
            $scope.deleteId = id;
        };

        $scope.marcar = function(item){//Se le pasa el objeto en lugar del indice debido a la ordenacion que lo deja mal
            if( item.Marcada == 1){
                item.Marcada = 0;
                var index = $scope.contabilidades.indexOf(item);
                $scope.contabilidades[index]= item;
                Contabilidad.marcar({id:item.Id});

            }else {
                item.Marcada =  1;
                var index = $scope.contabilidades.indexOf(item);
                $scope.contabilidades[index]= item;
                Contabilidad.marcar({id: item.Id});
            }
        }
        $scope.buscarBetween = function(){
            var inicio = $scope.inicio;
            var final = $scope.final;
            $scope.contabilidades = Contabilidad.between({inicio:inicio,final:final});
            $scope.contabilidades.$promise.then(function(result){
                $scope.contabilidades = result;
            })
        }
        $scope.loadData = function(){
            $scope.contabilidades = Contabilidad.getAll();
            $scope.contabilidades.$promise.then(function (result) {
                $scope.contabilidades = result;

            });
        }



  }]);
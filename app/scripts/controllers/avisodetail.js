'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:AvisodetailCtrl
 * @description
 * # AvisodetailCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('AvisodetailCtrl',['$scope','$routeParams', '$location','Aviso','Tarea','Incidencia','Persona','Comunidad', function ($scope,$routeParams,$location, Aviso, Tarea,Incidencia,Persona,Comunidad) {
        var actualDate = new Date();
        $scope.selectPersona= false;
        $scope.selectEmpresa= false;
        $scope.selectIncidencia= false;
        $scope.aviso = Aviso.get({id:$routeParams.id});
        $scope.aviso.$promise.then(function(result){
            $scope.aviso = result;
            $scope.selectedOption = $scope.aviso.Prioridad;

            $scope.aviso = result;
            $scope.asoc;
            if ($scope.aviso.ComunidadId!==null){
                $scope.button={
                    'radio': 3
                };
                if ($scope.aviso.ComunidadId!==0){
                    $scope.asoc =Comunidad.get({id:$scope.aviso.ComunidadId});

                    $scope.asoc.$promise.then(function(){
                        $scope.asociacion = $scope.asoc.Nombre;
                        $scope.selectPersona= false;
                        $scope.selectTarea= false;
                        $scope.selectComunidad= true;
                        $scope.selectIncidencia= false;
                    });
                }
            }else if ($scope.aviso.IncidenciaId!==null){
                $scope.button={
                    'radio': 2
                };
                if ($scope.aviso.IncidenciaId!==0){
                    $scope.asoc = Incidencia.get({id:$scope.aviso.IncidenciaId});
                    $scope.asoc.$promise.then(function(){
                        $scope.asociacion = $scope.asoc.Resumen;
                        $scope.selectPersona= false;
                        $scope.selectTarea= false;
                        $scope.selectComunidad= false;
                        $scope.selectIncidencia= true;
                    });
                }
            }else if($scope.aviso.PersonaId!==null) {
                $scope.button={
                    'radio': 0
                };
                if ($scope.aviso.PersonaId!==0){
                    $scope.asoc = Persona.getID({id:$scope.aviso.PersonaId});
                    $scope.asoc.$promise.then(function(){
                        $scope.asociacion = $scope.asoc.Nombre;
                        $scope.selectPersona= true;
                        $scope.selectTarea= false;
                        $scope.selectComunidad= false;
                        $scope.selectIncidencia= false;
                    });
                }
            }else if($scope.aviso.TareaId!==null) {
                $scope.button={
                    'radio': 1
                };
                if ($scope.aviso.TareaId!==0){
                    $scope.asoc = Tarea.getID({id:$scope.aviso.TareaId});
                    $scope.asoc.$promise.then(function(){
                        $scope.asociacion = $scope.asoc.Nombre;
                        $scope.selectPersona= false;
                        $scope.selectTarea= true;
                        $scope.selectComunidad= false;
                        $scope.selectIncidencia= false;
                    });
                }
            }
        });
        $scope.prioridadClass = function(selectedOption){
            return {
                'label-success': selectedOption==1,
                'label-primary': selectedOption==2,
                'label-info': selectedOption==3,
                'label-danger': selectedOption==5,
                'label-warning': selectedOption==4
            }
        }
        $scope.pri = '3';
        $scope.prioridadValues = [{'value': '1', 'label':'Poca'}, {'value': '2', 'label':'Algo'},{'value': '3', 'label':'Normal'},{'value': '4', 'label':'Importante'},{'value': '5', 'label':'Muy Importante'}];
        $scope.submit = function(){
            $scope.aviso.Prioridad = $scope.selectedOption;
            $scope.aviso = Aviso.update({id:$routeParams.id},$scope.aviso);
            $scope.aviso.$promise.then(function(){
                $location.path('/aviso');
            });
        };

        $scope.selectPersonaId= function(persona){
            $scope.selectPersona= true;
            $scope.selectTarea= false;
            $scope.selectComunidad= false;
            $scope.selectIncidencia= false;

            //$scope.asociacion = persona.Nombre;
            $scope.aviso.TareaId =null;
            $scope.aviso.ComunidadId =null;
            $scope.aviso.IncidenciaId = null;
            $scope.aviso.PersonaId = persona.Id;
        }
        $scope.selectComunidadId= function(comunidad){
            $scope.selectPersona= false;
            $scope.selectTarea= false;
            $scope.selectComunidad= true;
            $scope.selectIncidencia= false;

            //$scope.asociacion = comunidad.Nombre;
            $scope.aviso.TareaId =null;
            $scope.aviso.ComunidadId = comunidad.Id;
            $scope.aviso.PersonaId =null;
            $scope.aviso.IncidenciaId = null;
        }
        $scope.selectIncidenciaId= function(incidencia){
            $scope.selectPersona= false;
            $scope.selectTarea= false;
            $scope.selectComunidad= false;
            $scope.selectIncidencia= true;

            //$scope.asociacion = incidencia.Nombre;
            $scope.aviso.IncidenciaId = incidencia.Id;
            $scope.aviso.TareaId =null;
            $scope.aviso.ComunidadId =null;
            $scope.aviso.PersonaId = null;
        }
        $scope.selectTareaId= function(tarea){
            $scope.selectPersona= false;
            $scope.selectTarea= true;
            $scope.selectComunidad= false;
            $scope.selectIncidencia= false;

            //$scope.asociacion = tarea.Nombre;
            $scope.aviso.IncidenciaId = null;
            $scope.aviso.TareaId =tarea.Id;

            $scope.aviso.ComunidadId =null;
            $scope.aviso.PersonaId = null;
        }



        $scope.showAsociation = function(association){
            if(association=="persona"){
                $scope.selectPersona= true;
                $scope.selectTarea= false;
                $scope.selectComunidad= false;
                $scope.selectIncidencia= false;
                $scope.personas = Persona.getAll();
                $scope.personas.$promise.then(function(result){
                    $scope.personas = result;
                });
            }else if(association=="tarea") {
                $scope.selectPersona= false;
                $scope.selectTarea= true;
                $scope.selectComunidad= false;
                $scope.selectIncidencia= false;
                $scope.tareas = Tarea.getAll();
                $scope.tareas.$promise.then(function(result){
                    $scope.tareas = result;
                });
            }else if(association=="incidencia"){
                $scope.selectPersona= false;
                $scope.selectTarea= false;
                $scope.selectComunidad= false;
                $scope.selectIncidencia= true;
                $scope.incidencias = Incidencia.getAll(); //TODO
                $scope.incidencias.$promise.then(function(result){
                    $scope.incidencias = result;
                });
            }
            else if(association=="comunidad"){
                $scope.selectPersona= false;
                $scope.selectTarea= false;
                $scope.selectComunidad= true;
                $scope.selectIncidencia= false;
                $scope.comunidades = Comunidad.getAll(); //TODO
                $scope.comunidades.$promise.then(function(result){
                    $scope.incidencias = result;
                });
            }
        }
    }]);
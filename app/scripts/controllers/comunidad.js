'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:ComunidadCtrl
 * @description
 * # ComunidadCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp').controller('ComunidadCtrl', ['$scope', '$location' , '$routeParams', 'Comunidad' , function ($scope, $location, $routeParams, Comunidad) {
    $scope.comunidades = Comunidad.getAll();
    $scope.comunidades.$promise.then(function (result) {
        $scope.comunidades = result;
    });
    $scope.deleteId = '';

    $scope.edit = function ($id) {
        $location.path('/comunidad/view/' + $id);
    };
    $scope.delete = function ($id) {
        $scope.comunidades = Comunidad.delete({id: $id});
        $scope.comunidades.$promise.then(function () {
            $scope.comunidades = Comunidad.getAll();
        });
    };
    $scope.create = function () {
        $location.path('/comunidad/create');
    };
    $scope.getIdToRemove = function(id){
        console.log('Id a borrar: '+id);
        $scope.deleteId = id;
    };

}]);

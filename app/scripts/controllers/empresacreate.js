'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:EmpresacreateCtrl
 * @description
 * # EmpresacreateCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('EmpresacreateCtrl',['$scope', '$location','Empresa','Telefono', 'Comunidad','$alert', function ($scope,$location, Empresa,Telefono, Comunidad,$alert) {
        var actualDate = new Date();
        $scope.empresa = {
            ComunidadNombre: '',
            Contacto:'',
            Direccion:'',
            Cif:"",
            ComunidadId:0,
            Observaciones:"",
            Telefonos: []
        };
        var alert3 = $alert({title: 'Alerta!', content: "No has seleccionado una comunidad en la tabla de la Derecha", duration:2, placement: 'top', type: 'info', keyboard: true, show: false, container:"#alertComunidad"});
        $scope.submit = function(){
            if($scope.empresa.ComunidadId==0){
                alert3.show();
            }else {
                $scope.empresa = Empresa.create($scope.empresa);
                $scope.empresa.$promise.then(function(){
                    $location.path('/empresa');
                });
            }

        };


        $scope.comunidades = Comunidad.getAll();
        $scope.comunidades.$promise.then(function(result){
            $scope.comunidades = result;
        });
        $scope.selectComunidad = function(nombre, id){
            $scope.comunidadNombre = nombre;
            $scope.empresa.ComunidadId = id;
        };
        var alert1 = $alert({title: 'Alerta!', content: "Número repetido", duration:2, placement: 'top', type: 'info', keyboard: true, show: false, container:"#alert"});
        var alert2 = $alert({title: 'Alerta!', content: "No has introducido nada", duration:2, placement: 'top', type: 'info', keyboard: true, show: false, container:"#alert"});
        $scope.isPhoneRepeated = false;
        $scope.telefonos = [];
        $scope.addPhone = function(){
            $scope.isPhoneRepeated = false;
            if(_.contains($scope.telefonos,$scope.telefono)){
                alert1.show();

            }else if (_.isEmpty($scope.telefono)){

                alert2.show();

            }
            else {
                $scope.telefonos.push($scope.telefono);
                $scope.empresa.Telefonos = $scope.telefonos;

            }
            $scope.telefono ="";


        }
        $scope.deletePhone = function(){
            $scope.telefonos.pop();
            $scope.empresa.Telefonos = $scope.telefonos;

        }


    }]);

'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:TipocreateCtrl
 * @description
 * # TipocreateCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('TipocreateCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

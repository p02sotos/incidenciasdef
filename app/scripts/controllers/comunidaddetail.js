'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:ComunidaddetailCtrl
 * @description
 * # ComunidaddetailCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('ComunidadDetailCtrl',['$scope', '$location','$routeParams','Comunidad' ,function ($scope,$location,$routeParams,Comunidad) {
        $scope.comunidad = Comunidad.get({id:$routeParams.id});

        $scope.submit= function(){
            $scope.com = Comunidad.update({id:$routeParams.id},$scope.comunidad);
            $scope.com.$promise.then(function(){
                $location.path('/comunidad');
            });

        };
  }]);

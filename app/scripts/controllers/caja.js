'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:CajaCtrl
 * @description
 * # CajaCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('CajaCtrl',['$scope','$location' ,'$routeParams','Caja', function ($scope, $location, $routeParams, Caja) {
        $scope.filtro = '$';
        $scope.deleteId = '';
        $scope.isFilter = true;
        $scope.changeSearch = function(){
            $scope.isCollapsed = !$scope.isCollapsed;
            $scope.isFilter = !$scope.isFilter;
        }
        $scope.search = {Notas:'', 'Comunidad.Nombre':'', 'Marcada':'',Texto:'',$:''};
        $scope.cajas = Caja.getAll();
        $scope.cajas.$promise.then(function (result) {
            $scope.cajas = result;

        });
        $scope.radioModel = 'Todo';
        $scope.isCollapsed= true;

        $scope.edit = function ($id) {
            $location.path('/caja/view/' + $id);
        };
        $scope.toTipo = function () {
            $location.path('/tipo');
        };
        $scope.remove = function ($id) {
            console.log($id);
            $scope.cajas = Caja.delete({id: $id});
            $scope.cajas.$promise.then(function () {
                $scope.cajas = Caja.getAll();
            });
        };
        $scope.create = function () {
            $location.path('/caja/create');
        };
        $scope.viewMovimientos = function (id) {
            $location.path('/caja/movimiento/'+id);
        };
        $scope.changeFilterBy = function(filter){
            /*$scope.filtro = '$';
            $scope.search.Marcada = '';
            if(filter=='Marcada'){
                $scope.search.Marcada = true;
            }*/
            $scope.filtro = filter;
            $scope.isCollapsed =true;
            $scope.isFilter = true;
        };
        $scope.getIdToRemove = function(id){
            console.log('Id a borrar: '+id);
            $scope.deleteId = id;
        };

        $scope.buscarBetween = function(){
            var inicio = $scope.inicio;
            var final = $scope.final;
            $scope.cajas = Caja.between({inicio:inicio,final:final});
            $scope.cajas.$promise.then(function(result){
                $scope.cajas = result;
            })
        }
        $scope.loadData = function(){
            $scope.cajas = Caja.getAll();
            $scope.cajas.$promise.then(function (result) {
                $scope.cajas = result;

            });
        }
        $scope.createAviso = function(caja){
            alert("TODO");
        }
        $scope.createTarea = function(caja){
            alert("TODO");
        }

        $scope.marcar = function(item){//Se le pasa el objeto en lugar del indice debido a la ordenacion que lo deja mal
            if( item.Marcada == true){
                item.Marcada = false;
                var index = $scope.cajas.indexOf(item);
                $scope.cajas[index]= item;
                Caja.marcar({id:item.Id});

            }else {
                item.Marcada =  true;
                var index = $scope.cajas.indexOf(item);
                $scope.cajas[index]= item;
                Caja.marcar({id: item.Id});
            }
        }

        $scope.orderFields = [
            "Texto",
            "Comunidad",
            "Fecha"

        ]


        $scope.tooltips = {
            list: {
                "title": "Movimientos de la caja",
                "placement": 'right',
                delay: 500
            },
            aviso: {
                "title": "Crear Aviso a partir de esta caja",
                delay: 500
            },
            editar: {
                "title": "Editar caja",
                delay: 500
            },
            borrar: {
                "title": "Borrar"
            }
        };
    }]);
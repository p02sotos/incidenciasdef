'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:AvisoCtrl
 * @description
 * # AvisoCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('AvisoCtrl',['$scope','$location' ,'$routeParams','Aviso', function ($scope, $location, $routeParams, Aviso) {
        $scope.filtro = '$';
        $scope.deleteId = '';
        $scope.isFilter = true;
        $scope.changeSearch = function(){
            $scope.isCollapsed = !$scope.isCollapsed;
            $scope.isFilter = !$scope.isFilter;
        }
        $scope.search = {Nombre:'', 'Descripcion':'', FechaAviso:'',Marcada:'',Prioridad:'',$:''};
        $scope.avisos = Aviso.getAll();
        $scope.avisos.$promise.then(function (result) {
            $scope.avisos = result;

        });

        $scope.radioModel = 'Todo';
        $scope.isCollapsed= true;

        $scope.edit = function ($id) {
            $location.path('/aviso/view/' + $id);
        };
        $scope.toTipo = function () {
            $location.path('/tipo');
        };
        $scope.remove = function ($id) {

            $scope.avisos = Aviso.delete({id: $id});
            $scope.avisos.$promise.then(function () {
                $scope.avisos = Aviso.getAll();
            });
        };
        $scope.calendar = function () {
            alert ('To calendar'); //TODO
        };
        $scope.create = function () {
            $location.path('/aviso/create');
        };
        $scope.changeFilterBy = function(filter){
            $scope.filtro = filter;
            $scope.isCollapsed =true;
            $scope.isFilter = true;
        };
        $scope.getIdToRemove = function(id){
            console.log('Id a borrar: '+id);
            $scope.deleteId = id;
        };

        $scope.prioridadClass = function(aviso){
            return {
                'label-success': aviso.Prioridad==1,
                'label-primary': aviso.Prioridad==2,
                'label-info': aviso.Prioridad==3,
                'label-danger': aviso.Prioridad==5,
                'label-warning': aviso.Prioridad==4
            }
        }


        $scope.marcar = function(item){//Se le pasa el objeto en lugar del indice debido a la ordenacion que lo deja mal
            if( item.Marcada == 1){
                item.Marcada = 0;
                var index = $scope.avisos.indexOf(item);
                $scope.avisos[index]= item;
                Aviso.marcar({id:item.Id});

            }else {
                item.Marcada =  1;
                var index = $scope.avisos.indexOf(item);
                $scope.avisos[index]= item;
                Aviso.marcar({id: item.Id});
            }
        }
        $scope.buscarBetween = function(){
            var inicio = $scope.inicio;
            var final = $scope.final;
            $scope.avisos = Aviso.between({inicio:inicio,final:final});
            $scope.avisos.$promise.then(function(result){
                $scope.avisos = result;
            })
        }
        $scope.loadData = function(){
            $scope.avisos = Aviso.getAll();
            $scope.avisos.$promise.then(function (result) {
                $scope.avisos = result;

            });
        }



    }]);
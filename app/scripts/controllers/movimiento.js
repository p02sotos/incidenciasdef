'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:MovimientoCtrl
 * @description
 * # MovimientoCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('MovimientoCtrl',['$scope','$route','$location' ,'$routeParams','Caja','Movimiento', function ($scope, $route,$location, $routeParams,Caja,  Movimiento) {
        $scope.filtro = '$';
        $scope.deleteId = '';
        $scope.isFilter = true;
        $scope.changeSearch = function(){
            $scope.isCollapsed = !$scope.isCollapsed;
            $scope.isFilter = !$scope.isFilter;
        }
        $scope.search = {'FechaMovimiento':'', 'Concepto':'', 'Cantidad':'',$:''};
        $scope.movimientos = Movimiento.getAll({id:$routeParams.id});
        $scope.movimientos.$promise.then(function (result) {
            $scope.movimientos = result;
            var saldo = 0;
            angular.forEach ($scope.movimientos, function(value,key){
                    saldo+= value.Cantidad;
            });
            $scope.saldo = saldo;


            // $scope.movimientosNotNull =  objectNullToEmpty($scope.movimientos);
        });

        //TODO  crear un filtro para esto
        /*var objectNullToEmpty = function (obj) {
         for (var key in obj) {
         if (obj[key] === null) {
         obj[key] = '';
         }
         if (typeof obj[key] === 'object') {
         objectNullToEmpty(obj[key]);
         }
         }
         return obj;
         };*/

        $scope.radioModel = 'Todo';
        $scope.isCollapsed= true;

        $scope.edit = function (movimiento) {
            $scope.movimientoActual = movimiento;

        };
        $scope.toTipo = function () {
            $location.path('/tipo');
        };
        $scope.remove = function ($id) {
            console.log($id);
            $scope.movimientos = Movimiento.delete({id: $id});
            $scope.movimientos.$promise.then(function () {
                $scope.movimientos = Movimiento.getAll({id:$routeParams.id});
                $route.reload();



            });
        };
        $scope.create = function () {
            $scope.creating = true;
            $scope.movimientoActual= {
                Concepto: '',
                Cantidad:'',
                Fecha: ''
            }

        };

        $scope.creating = false;
        $scope.save = function(){
            if($scope.creating){
                $scope.creating = false;
                $scope.movimientoActual.CajaId = $routeParams.id;
                $scope.mov = Movimiento.create($scope.movimientoActual);
                $scope.mov.$promise.then(function(){
                    $route.reload();
                });
            }else {
                $scope.creating = false;
                $scope.movimientoActual.CajaId = $routeParams.id;
                $scope.mov = Movimiento.update({id:$scope.movimientoActual.Id},$scope.movimientoActual);
                $scope.mov.$promise.then(function(){
                    $route.reload();
                });
            }
        }
        $scope.changeFilterBy = function(filter){
            /*$scope.filtro = '$';
             $scope.search.Marcada = '';
             if(filter=='Marcada'){
             $scope.search.Marcada = true;
             }*/
            $scope.filtro = filter;
            $scope.isCollapsed =true;
            $scope.isFilter = true;
        };
        $scope.getIdToRemove = function(id){
            console.log('Id a borrar: '+id);
            $scope.deleteId = id;
        };

        $scope.buscarBetween = function(){
            var inicio = $scope.inicio;
            var final = $scope.final;
            $scope.movimientos = Movimiento.between({inicio:inicio,final:final});
            $scope.movimientos.$promise.then(function(result){
                $scope.movimientos = result;
            })
        }
        $scope.loadData = function(){
            $scope.movimientos = Movimiento.getAll({id:$routeParams.id});
            $scope.movimientos.$promise.then(function (result) {
                $scope.movimientos = result;

            });
        }
        $scope.createAviso = function(movimiento){
            alert("TODO");
        }
        $scope.createTarea = function(movimiento){
            alert("TODO");
        }

        $scope.marcar = function(item){//Se le pasa el objeto en lugar del indice debido a la ordenacion que lo deja mal
            if( item.Marcada == true){
                item.Marcada = false;
                var index = $scope.movimientos.indexOf(item);
                $scope.movimientos[index]= item;
                movimiento.marcar({id:item.Id});

            }else {
                item.Marcada =  true;
                var index = $scope.movimientos.indexOf(item);
                $scope.movimientos[index]= item;
                movimiento.marcar({id: item.Id});
            }
        }

        $scope.orderFields = [
            "Texto",
            "Comunidad",
            "Fecha"

        ]


        $scope.tooltips = {
            list: {
                "title": "Movimientos de la movimiento",
                "placement": 'right',
                delay: 500
            },
            aviso: {
                "title": "Crear Aviso a partir de esta movimiento",
                delay: 500
            },
            editar: {
                "title": "Editar movimiento",
                delay: 500
            },
            borrar: {
                "title": "Borrar"
            }
        };
    }]);
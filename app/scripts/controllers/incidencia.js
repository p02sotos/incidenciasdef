'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:IncidenciaCtrl
 * @description
 * # IncidenciaCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('IncidenciaCtrl',['$scope','$location' ,'$routeParams','Seguimiento','Telefono','Incidencia', function ($scope, $location, $routeParams, Seguimiento, Telefono, Incidencia) {
        $scope.filtro = '$';
        $scope.deleteId = '';
        $scope.isFilter = true;
        $scope.changeSearch = function(){
            $scope.isCollapsed = !$scope.isCollapsed;
            $scope.isFilter = !$scope.isFilter;
        }
        $scope.search = {Breve:'', 'Descripcion':'',Expediente:'', 'FechaIncidencia':'',Marcada:'',Prioridad:'',Atencion:'',PersonaNombre:'',Telefonos:'','Resuelta':'false',$:''};
        $scope.incidencias = Incidencia.getAll();
        $scope.incidencias.$promise.then(function (result) {
            $scope.incidencias = result;
            $scope.phones

        });
        $scope.purgar= false
        $scope.reverse= true;

        $scope.radioModel = 'Todo';
        $scope.isCollapsed= true;

        $scope.edit = function ($id) {
            $location.path('/incidencia/view/' + $id);
        };
        $scope.toTipo = function () {
            $location.path('/tipo');
        };
        $scope.remove = function ($id) {
            $scope.incidencias = Incidencia.delete({id: $id});
            $scope.incidencias.$promise.then(function () {
                $scope.incidencias = Incidencia.getAll();
            });
        };
        $scope.purgueConfirm = function(id){
            $scope.deleteId = id;

        }
        $scope.purgue = function ($id) {

            $scope.incidencias = Incidencia.purgue({id: $id});
            $scope.incidencias.$promise.then(function () {
                $scope.incidencias = Incidencia.getTrash();
                $scope.purgar= false;
            });
        }
        $scope.restore = function ($id) {
            $scope.incidencias = Incidencia.restore({id: $id});
            $scope.incidencias.$promise.then(function () {
                $scope.incidencias = Incidencia.getTrash();
            });
        }
        $scope.toTrash = function(){
            $scope.incidencias = Incidencia.getTrash();
            $scope.incidencias.$promise.then(function (result) {
                $scope.incidencias = result;
                $scope.purgar= true;
            });
        }

        $scope.remove = function ($id) {
            $scope.incidencias = Incidencia.delete({id: $id});
            $scope.incidencias.$promise.then(function () {
                $scope.incidencias = Incidencia.getAll();
                $scope.purgar= false;
            });
        };
        $scope.calendar = function () {
            alert ('To calendar'); //TODO
        };
        $scope.create = function () {
            $location.path('/incidencia/create');
        };
        $scope.changeFilterBy = function(filter){
            $scope.filtro = filter;
            $scope.isCollapsed =true;
            $scope.isFilter = true;
        };
        $scope.getIdToRemove = function(id){
            console.log('Id a borrar: '+id);
            $scope.deleteId = id;
        };


        $scope.seguimientoOpen = false;
        $scope.loadSeguimiento = function(id){
            if($scope.seguimientoOpen){
                $scope.seguimientoOpen = false;
                $scope.seguimientos = [];
            }else {
                $scope.seguimientoOpen = true;
                $scope.seguimientos = Seguimiento.getAll({id:id});
                $scope.seguimientos.$promise.then(function(result){
                    $scope.seguimientos = result;
                    $scope.purgar= false;
                })
            }

        };

        $scope.prioridadClass = function(incidencia){
            return {
                'label-success': incidencia.Prioridad==1,
                'label-primary': incidencia.Prioridad==2,
                'label-info': incidencia.Prioridad==3,
                'label-danger': incidencia.Prioridad==5,
                'label-warning': incidencia.Prioridad==4
            }
        }
        $scope.panelClass = function(incidencia){
            return {
                'panel-danger': incidencia.Marcada==1&&incidencia.Resuelta==0,
                'panel-primary':incidencia.Marcada==1&&incidencia.Resuelta==1,
                'panel-default': incidencia.Marcada==0&&incidencia.Resuelta==0,
                'panel-success': incidencia.Marcada==0&&incidencia.Resuelta==1

            }
        }


        $scope.marcar = function(item){//Se le pasa el objeto en lugar del indice debido a la ordenacion que lo deja mal
            if( item.Marcada == 1){
                item.Marcada = 0;
                var index = $scope.incidencias.indexOf(item);
                $scope.incidencias[index]= item;
                Incidencia.marcar({id:item.Id});

            }else {
                item.Marcada =  1;
                var index = $scope.incidencias.indexOf(item);
                $scope.incidencias[index]= item;
                Incidencia.marcar({id: item.Id});
            }
        }
        $scope.resolve = function(item){//Se le pasa el objeto en lugar del indice debido a la ordenacion que lo deja mal
            if( item.Resuelta == 1){
                item.Resuelta = 0;
                var index = $scope.incidencias.indexOf(item);
                $scope.incidencias[index]= item;
                Incidencia.resolve({id:item.Id});

            }else {
                item.Resuelta =  1;
                var index = $scope.incidencias.indexOf(item);
                $scope.incidencias[index]= item;
                Incidencia.resolve({id: item.Id});
            }
        }
        $scope.buscarBetween = function(){
            var inicio = $scope.inicio;
            var final = $scope.final;
            $scope.incidencias = Incidencia.between({inicio:inicio,final:final});
            $scope.incidencias.$promise.then(function(result){
                $scope.incidencias = result;
            })
        }
        $scope.loadData = function(){
            $scope.incidencias = Incidencia.getAll();
            $scope.incidencias.$promise.then(function (result) {
                $scope.incidencias = result;
                $scope.purgar= false;

            });
        }

        $scope.orderFields = [
            "Breve",
            "Comunidad",
            "FechaIncidencia",
            "Prioridad"            

        ]
         $scope.changeOrder = function(){
           if ($scope.reverse){
               $scope.reverse = false;

           }else {
               $scope.reverse = true;
           }

        }


        $scope.tooltips = {
            list: {
                "title": "Movimientos de la caja",
                "placement": 'right',
                delay: 500
            },
            aviso: {
                "title": "Crear Aviso a partir de esta caja",
                delay: 500
            },
            editar: {
                "title": "Editar caja",
                delay: 500
            },
            borrar: {
                "title": "Borrar"
            }
        };



    }]);
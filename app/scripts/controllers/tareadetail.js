'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:TareadetailCtrl
 * @description
 * # TareadetailCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('TareadetailCtrl',['$scope', '$routeParams','$location','Tarea', 'Incidencia', function ($scope,$routeParams,$location, Tarea, Incidencia) {
        $scope.tarea = Tarea.get({id:$routeParams.id});
        $scope.tarea.$promise.then(function(result){
            $scope.tarea = result;
            $scope.selectedOption = $scope.tarea.Prioridad;


        });
        $scope.prioridadText = function(){
            switch($scope.selectedOption){
                case 1:
                    return "Poca";
                case 2:
                    return "Algo";
                case 3:
                    return "Normal";
                case 4:
                    return "Importante";
                case 5:
                    return "Muy importante";

            }
        }


        $scope.prioridadClass = function(selectedOption){
            return {
                'label-success': selectedOption==1,
                'label-primary': selectedOption==2,
                'label-info': selectedOption==3,
                'label-danger': selectedOption==5,
                'label-warning': selectedOption==4
            }
        }


        $scope.prioridadValues = [{'value': '1', 'label':'Poca'}, {'value': '2', 'label':'Algo'},{'value': '3', 'label':'Normal'},{'value': '4', 'label':'Importante'},{'value': '5', 'label':'Muy Importante'}];
        $scope.submit = function(){

            $scope.tarea.Prioridad = $scope.selectedOption;
            $scope.tarea = Tarea.update({id:$routeParams.id},$scope.tarea);
            $scope.tarea.$promise.then(function(){
                $location.path('/tarea');
            });
        };
        //$scope.tarea.Prioridad = '3';


        $scope.incidencias = Incidencia.getAll();
        $scope.incidencias.$promise.then(function(result){
            $scope.incidencias = result;
        });
        $scope.selectIncidencia = function(nombre, id){

            $scope.tarea.IncidenciaId = id;
        };



    }]);
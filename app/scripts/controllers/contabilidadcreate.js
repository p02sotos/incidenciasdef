'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:ContabilidadcreateCtrl
 * @description
 * # ContabilidadcreateCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('ContabilidadcreateCtrl',['$scope', '$location','Contabilidad', 'Comunidad', function ($scope,$location, Contabilidad, Comunidad) {
        var actualDate = new Date();
        $scope.contabilidad = {
            Resumen: '',
            Notas:'',
            FechaEntrega: actualDate,
            ComunidadId:''
        };
        $scope.submit = function(){

            $scope.contabilidad = Contabilidad.create($scope.contabilidad);
            $scope.contabilidad.$promise.then(function(){
                $location.path('/contabilidad');
            });
        };


        $scope.comunidades = Comunidad.getAll();
        $scope.comunidades.$promise.then(function(result){
            $scope.comunidades = result;
        });
        $scope.selectComunidad = function(nombre, id){
            $scope.comunidadNombre = nombre;
            $scope.contabilidad.ComunidadId = id;
        };



    }]);

'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:CajadetailCtrl
 * @description
 * # CajadetailCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('CajadetailCtrl',['$scope', '$location','$routeParams','Caja','Comunidad', function ($scope,$location,$routeParams,Caja, Comunidad) {

        $scope.caja = Caja.get({id:$routeParams.id});
        $scope.caja.$promise.then(function(result){
            $scope.caja = result;
            $scope.comunidadNombre = $scope.caja['Comunidad.Nombre'];
        });


        $scope.submit= function(){

            $scope.cont = Caja.update({id:$routeParams.id},$scope.caja);
            $scope.cont.$promise.then(function(){
                $location.path('/caja');
            });

        };
        $scope.comunidades = Comunidad.getAll();

        $scope.comunidades.$promise.then(function(result){
            $scope.comunidades = result;

        });

        $scope.selectComunidad = function(nombre, id){
            $scope.comunidadNombre = nombre;
            $scope.caja['Comunidad.Id'] = id;

        };


    }]);


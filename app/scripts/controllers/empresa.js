'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:EmpresaCtrl
 * @description
 * # EmpresaCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('EmpresaCtrl',['$scope','$location' ,'$routeParams','Empresa', function ($scope, $location, $routeParams, Empresa) {
        $scope.filtro = '$';
        $scope.deleteId = '';
        $scope.isFilter = true;
        $scope.changeSearch = function(){
            $scope.isCollapsed = !$scope.isCollapsed;
            $scope.isFilter = !$scope.isFilter;
        }
        $scope.search = {Nombre: '',Telefonos:'','ComunidadNombre':'', 'Marcada':'',Observaciones:'',Cif:'',Contacto:'',$:''};
        $scope.empresas = Empresa.getWithPhones();
        $scope.empresas.$promise.then(function (result) {
            $scope.empresas = result;

        });
        $scope.radioModel = 'Todo';
        $scope.isCollapsed= true;

        $scope.edit = function ($id) {
            $location.path('/empresa/view/' + $id);
        };
        $scope.toTipo = function () {
            $location.path('/tipo');
        };
        $scope.remove = function ($id) {
            console.log($id);
            $scope.empresas = Empresa.delete({id: $id});
            $scope.empresas.$promise.then(function () {
                $scope.empresas = Empresa.getAll();
            });
        };
        $scope.create = function () {
            $location.path('/empresa/create');
        };
        $scope.changeFilterBy = function(filter){
            /*$scope.filtro = '$';
             $scope.search.Marcada = '';
             if(filter=='Marcada'){
             $scope.search.Marcada = true;
             }*/
            $scope.filtro = filter;
            $scope.isCollapsed =true;
            $scope.isFilter = true;
        };
        $scope.getIdToRemove = function(id){
            console.log('Id a borrar: '+id);
            $scope.deleteId = id;
        };

        $scope.buscarBetween = function(){
            var inicio = $scope.inicio;
            var final = $scope.final;
            $scope.empresas = Empresa.between({inicio:inicio,final:final});
            $scope.empresas.$promise.then(function(result){
                $scope.empresas = result;
            })
        }
        $scope.loadData = function(){
            $scope.empresas = Empresa.getWithPhones();
            $scope.empresas.$promise.then(function (result) {
                $scope.empresas = result;

            });
        }
        $scope.createAviso = function(empresa){
            alert("TODO");
        }
        $scope.createTarea = function(empresa){
            alert("TODO");
        }

        $scope.marcar = function(item){//Se le pasa el objeto en lugar del indice debido a la ordenacion que lo deja mal
            if( item.Marcada == true){
                item.Marcada = false;
                var index = $scope.empresas.indexOf(item);
                $scope.empresas[index]= item;
                Empresa.marcar({id:item.Id});

            }else {
                item.Marcada =  true;
                var index = $scope.empresas.indexOf(item);
                $scope.empresas[index]= item;
                Empresa.marcar({id: item.Id});
            }
        }

        $scope.orderFields = [
            "Texto",
            "Comunidad",
            "Fecha"

        ]


        $scope.tooltips = {
            list: {
                "title": "Movimientos de la empresa",
                "placement": 'right',
                delay: 500
            },
            aviso: {
                "title": "Crear Aviso a partir de esta empresa",
                delay: 500
            },
            editar: {
                "title": "Editar empresa",
                delay: 500
            },
            borrar: {
                "title": "Borrar"
            }
        };
    }]);
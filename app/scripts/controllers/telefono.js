'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:TelefonoCtrl
 * @description
 * # TelefonoCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('TelefonoCtrl',['$scope','$location' ,'$routeParams','Telefono', function ($scope, $location, $routeParams, Telefono) {
        $scope.filtro = '$';
        $scope.deleteId = '';
        $scope.isFilter = true;
        $scope.changeSearch = function(){
            $scope.isCollapsed = !$scope.isCollapsed;
            $scope.isFilter = !$scope.isFilter;
        }
        $scope.search = {'Telefono':'', 'IncidenciaId':'', 'Tipo':'', 'EmpresaNombre':'', 'PersonaNombre':'','Marcada':'',$:''};
        $scope.telefonos = Telefono.getAll();
        $scope.telefonos.$promise.then(function (result) {
            $scope.telefonos = result;


           // $scope.telefonosNotNull =  objectNullToEmpty($scope.telefonos);
        });

        //TODO  crear un filtro para esto
        /*var objectNullToEmpty = function (obj) {
            for (var key in obj) {
                if (obj[key] === null) {
                    obj[key] = '';
                }
                if (typeof obj[key] === 'object') {
                    objectNullToEmpty(obj[key]);
                }
            }
            return obj;
        };*/

        $scope.radioModel = 'Todo';
        $scope.isCollapsed= true;

        $scope.edit = function ($id) {
            $location.path('/telefono/view/' + $id);
        };
        $scope.toTipo = function () {
            $location.path('/tipo');
        };
        $scope.remove = function ($id) {
            console.log($id);
            $scope.telefonos = Telefono.delete({id: $id});
            $scope.telefonos.$promise.then(function () {
                $scope.telefonos = Telefono.getAll();
            });
        };
        $scope.create = function () {
            $location.path('/telefono/create');
        };
        $scope.changeFilterBy = function(filter){
            /*$scope.filtro = '$';
             $scope.search.Marcada = '';
             if(filter=='Marcada'){
             $scope.search.Marcada = true;
             }*/
            $scope.filtro = filter;
            $scope.isCollapsed =true;
            $scope.isFilter = true;
        };
        $scope.getIdToRemove = function(id){
            console.log('Id a borrar: '+id);
            $scope.deleteId = id;
        };

        $scope.buscarBetween = function(){
            var inicio = $scope.inicio;
            var final = $scope.final;
            $scope.telefonos = Telefono.between({inicio:inicio,final:final});
            $scope.telefonos.$promise.then(function(result){
                $scope.telefonos = result;
            })
        }
        $scope.loadData = function(){
            $scope.telefonos = Telefono.getAll();
            $scope.telefonos.$promise.then(function (result) {
                $scope.telefonos = result;

            });
        }
        $scope.createAviso = function(telefono){
            alert("TODO");
        }
        $scope.createTarea = function(telefono){
            alert("TODO");
        }

        $scope.marcar = function(item){//Se le pasa el objeto en lugar del indice debido a la ordenacion que lo deja mal
            if( item.Marcada == true){
                item.Marcada = false;
                var index = $scope.telefonos.indexOf(item);
                $scope.telefonos[index]= item;
                Telefono.marcar({id:item.Id});

            }else {
                item.Marcada =  true;
                var index = $scope.telefonos.indexOf(item);
                $scope.telefonos[index]= item;
                Telefono.marcar({id: item.Id});
            }
        }

        $scope.orderFields = [
            "Texto",
            "Comunidad",
            "Fecha"

        ]


        $scope.tooltips = {
            list: {
                "title": "Movimientos de la telefono",
                "placement": 'right',
                delay: 500
            },
            aviso: {
                "title": "Crear Aviso a partir de esta telefono",
                delay: 500
            },
            editar: {
                "title": "Editar telefono",
                delay: 500
            },
            borrar: {
                "title": "Borrar"
            }
        };
    }]);
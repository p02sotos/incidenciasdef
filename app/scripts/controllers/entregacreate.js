'use strict';
/**
 * Created by Ser on 14/06/14.
 */
angular.module('incidenciasDefApp')
    .controller('EntregaCreateCtrl',['$scope', '$location','Entrega', 'Comunidad','Tipo' , function ($scope,$location, Entrega, Comunidad, Tipo) {
        var actualDate = new Date();
        $scope.entrega = {
            Nombre: '',
            Descripcion:'',
            TipoId: '',
            FechaEntrega: actualDate,
            ComunidadId:''
        };
        $scope.submit = function(){
            $scope.entrega['TipoId']= $scope.selectedOption;
            $scope.entrega = Entrega.create($scope.entrega);
            $scope.entrega.$promise.then(function(){
                $location.path('/entrega');
            });
        };
        $scope.tipos = Tipo.getAll();
        $scope.tipos.$promise.then(function(result){
            $scope.tipos = result;
        });
        $scope.comunidades = Comunidad.getAll();
        $scope.comunidades.$promise.then(function(result){
            $scope.comunidades = result;
        });
        $scope.selectComunidad = function(nombre, id){
          $scope.comunidadNombre = nombre;
            $scope.entrega.ComunidadId = id;
        };




    }]);

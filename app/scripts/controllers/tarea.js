'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:TareaCtrl
 * @description
 * # TareaCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('TareaCtrl',['$scope','$location' ,'$routeParams','Tarea', function ($scope, $location, $routeParams, Tarea) {
        $scope.filtro = '$';
        $scope.deleteId = '';
        $scope.isFilter = true;
        $scope.changeSearch = function(){
            $scope.isCollapsed = !$scope.isCollapsed;
            $scope.isFilter = !$scope.isFilter;
        }
        $scope.search = {Nombre:'', 'Descripcion':'', 'FechaTarea':'',Marcada:'',Prioridad:'',Realizada:'false',$:''};
        $scope.tareas = Tarea.getAll();
        $scope.tareas.$promise.then(function (result) {
            $scope.tareas = result;

        });

        $scope.radioModel = 'Todo';
        $scope.isCollapsed= true;

        $scope.edit = function ($id) {
            $location.path('/tarea/view/' + $id);
        };
        $scope.toTipo = function () {
            $location.path('/tipo');
        };
        $scope.purgar= false;

        $scope.remove = function ($id) {
            $scope.tareas = Tarea.delete({id: $id});
            $scope.tareas.$promise.then(function () {
                $scope.tareas = Tarea.getAll();
            });
        };
        $scope.purgueConfirm = function(id){
            $scope.deleteId = id;

        }
        $scope.purgue = function ($id) {

            $scope.tareas = Tarea.purgue({id: $id});
            $scope.tareas.$promise.then(function () {
                $scope.tareas = Tarea.getTrash();
                $scope.purgar= false;
            });
        }
        $scope.restore = function ($id) {
            $scope.tareas = Tarea.restore({id: $id});
            $scope.tareas.$promise.then(function () {
                $scope.tareas = Tarea.getTrash();
            });
        }

        $scope.toTrash = function(){
            $scope.tareas = Tarea.getTrash();
            $scope.tareas.$promise.then(function (result) {
                $scope.tareas = result;
                $scope.purgar= true;
            });
        }
        $scope.calendar = function () {
            alert ('To calendar'); //TODO
        };
        $scope.create = function () {
            $location.path('/tarea/create');
        };
        $scope.changeFilterBy = function(filter){
            $scope.filtro = filter;
            $scope.isCollapsed =true;
            $scope.isFilter = true;
        };
        $scope.getIdToRemove = function(id){
            console.log('Id a borrar: '+id);
            $scope.deleteId = id;
        };

        $scope.prioridadClass = function(tarea){
            return {
                'label-success': tarea.Prioridad==1,
                'label-primary': tarea.Prioridad==2,
                'label-info': tarea.Prioridad==3,
                'label-danger': tarea.Prioridad==5,
                'label-warning': tarea.Prioridad==4
            }
        }


        $scope.marcar = function(item){//Se le pasa el objeto en lugar del indice debido a la ordenacion que lo deja mal
            if( item.Marcada){
                item.Marcada = false;
                var index = $scope.tareas.indexOf(item);
                $scope.tareas[index]= item;
                Tarea.marcar({id:item.Id});

            }else {
                item.Marcada =  true;
                var index = $scope.tareas.indexOf(item);
                $scope.tareas[index]= item;
                Tarea.marcar({id: item.Id});
            }
        }

        $scope.panelClass = function(tarea){
            return {
                'panel-danger': tarea.Marcada==1&&tarea.Realizada==0,
                'panel-primary':tarea.Marcada==1&&tarea.Realizada==1,
                'panel-default': tarea.Marcada==0&&tarea.Realizada==0,
                'panel-success': tarea.Marcada==0&&tarea.Realizada==1

            }
        }

        $scope.realizar = function(item){//Se le pasa el objeto en lugar del indice debido a la ordenacion que lo deja mal
            if(item.Realizada){
                item.Realizada = false;
                var index = $scope.tareas.indexOf(item);
                $scope.tareas[index]= item;
                Tarea.resolve({id:item.Id});

            }else {
                item.Realizada =  true;
                var index = $scope.tareas.indexOf(item);
                $scope.tareas[index]= item;
                Tarea.resolve({id: item.Id});
            }
        }
        $scope.buscarBetween = function(){
            var inicio = $scope.inicio;
            var final = $scope.final;
            $scope.tareas = Tarea.between({inicio:inicio,final:final});
            $scope.tareas.$promise.then(function(result){
                $scope.tareas = result;
            })
        }
        $scope.loadData = function(){
            $scope.tareas = Tarea.getAll();
            $scope.tareas.$promise.then(function (result) {
                $scope.tareas = result;
                $scope.purgar= false;
            });
        }



    }]);
'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:AvisocreateCtrl
 * @description
 * # AvisocreateCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('AvisocreateCtrl',['$scope', '$location','Aviso','Tarea','Incidencia','Persona','Comunidad', function ($scope,$location, Aviso, Tarea,Incidencia,Persona,Comunidad) {
        var actualDate = new Date();
        $scope.selectPersona= false;
        $scope.selectEmpresa= false;
        $scope.selectIncidencia= false;
        $scope.aviso = {
            TareaId:'',
            IncidenciaId: '',
            ComunidadId:'',
            PersonaId:'',
            Descripcion:'',
            FechaAviso:'',
            Nombre:'',
            Prioridad:''


        };
        $scope.prioridadClass = function(selectedOption){
            return {
                'label-success': selectedOption==1,
                'label-primary': selectedOption==2,
                'label-info': selectedOption==3,
                'label-danger': selectedOption==5,
                'label-warning': selectedOption==4
            }
        }
        $scope.pri = '3';
        $scope.prioridadValues = [{'value': '1', 'label':'Poca'}, {'value': '2', 'label':'Algo'},{'value': '3', 'label':'Normal'},{'value': '4', 'label':'Importante'},{'value': '5', 'label':'Muy Importante'}];
        $scope.submit = function(){
            $scope.aviso.Prioridad = $scope.selectedOption;
            $scope.aviso = Aviso.create($scope.aviso);
            $scope.aviso.$promise.then(function(){
                $location.path('/aviso');
            });
        };

        $scope.selectPersonaId= function(persona){

            $scope.asociacion = persona.Nombre;
            $scope.aviso.TareaId =null;
            $scope.aviso.ComunidadId =null;
            $scope.aviso.IncidenciaId = null;
            $scope.aviso.PersonaId = persona.Id;
        }
        $scope.selectComunidadId= function(comunidad){

            $scope.asociacion = comunidad.Nombre;
            $scope.aviso.TareaId =null;
            $scope.aviso.ComunidadId = comunidad.Id;
            $scope.aviso.PersonaId =null;
            $scope.aviso.IncidenciaId = null;
        }
        $scope.selectIncidenciaId= function(incidencia){

            $scope.asociacion = incidencia.Breve;
            $scope.aviso.IncidenciaId = incidencia.Id;
            $scope.aviso.TareaId =null;
            $scope.aviso.ComunidadId =null;
            $scope.aviso.PersonaId = null;
        }
        $scope.selectTareaId= function(tarea){

            $scope.asociacion = tarea.Nombre;
            $scope.aviso.IncidenciaId = null;
            $scope.aviso.TareaId =tarea.Id;

            $scope.aviso.ComunidadId =null;
            $scope.aviso.PersonaId = null;
        }



        $scope.showAsociation = function(association){
            if(association=="persona"){
                $scope.selectPersona= true;
                $scope.selectTarea= false;
                $scope.selectComunidad= false;
                $scope.selectIncidencia= false;
                $scope.personas = Persona.getAll();
                $scope.personas.$promise.then(function(result){
                    $scope.personas = result;
                });
            }else if(association=="tarea") {
                $scope.selectPersona= false;
                $scope.selectTarea= true;
                $scope.selectComunidad= false;
                $scope.selectIncidencia= false;
                $scope.tareas = Tarea.getAll();
                $scope.tareas.$promise.then(function(result){
                    $scope.tareas = result;
                });
            }else if(association=="incidencia"){
                $scope.selectPersona= false;
                $scope.selectTarea= false;
                $scope.selectComunidad= false;
                $scope.selectIncidencia= true;
                $scope.incidencias = Incidencia.getAll(); //TODO
                $scope.incidencias.$promise.then(function(result){
                    $scope.incidencias = result;
                });
            }
            else if(association=="comunidad"){
                $scope.selectPersona= false;
                $scope.selectTarea= false;
                $scope.selectComunidad= true;
                $scope.selectIncidencia= false;
                $scope.comunidades = Comunidad.getAll(); //TODO
                $scope.comunidades.$promise.then(function(result){
                    $scope.incidencias = result;
                });
            }
        }
    }]);
'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:PersonaCtrl
 * @description
 * # PersonaCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('PersonaCtrl',['$scope','$location' ,'$routeParams','Persona', function ($scope, $location, $routeParams, Persona) {
        $scope.filtro = '$';
        $scope.deleteId = '';
        $scope.isFilter = true;
        $scope.changeSearch = function(){
            $scope.isCollapsed = !$scope.isCollapsed;
            $scope.isFilter = !$scope.isFilter;
        }
        $scope.search = {'Nombre':'', 'ComunidadNombre':'', 'Telefonos':'','Marcada':'',$:''};
        $scope.personas = Persona.getWithPhones();
        $scope.personas.$promise.then(function (result) {
            $scope.personas = result;

        });
        $scope.radioModel = 'Todo';
        $scope.isCollapsed= true;

        $scope.edit = function ($id) {
            $location.path('/persona/view/' + $id);
        };
        $scope.toTipo = function () {
            $location.path('/tipo');
        };
        $scope.remove = function ($id) {
            console.log($id);
            $scope.personas = Persona.delete({id: $id});
            $scope.personas.$promise.then(function () {
                $scope.personas = Persona.getAll();
            });
        };
        $scope.create = function () {
            $location.path('/persona/create');
        };
        $scope.changeFilterBy = function(filter){
            /*$scope.filtro = '$';
             $scope.search.Marcada = '';
             if(filter=='Marcada'){
             $scope.search.Marcada = true;
             }*/
            $scope.filtro = filter;
            $scope.isCollapsed =true;
            $scope.isFilter = true;
        };
        $scope.getIdToRemove = function(id){
            console.log('Id a borrar: '+id);
            $scope.deleteId = id;
        };

        $scope.buscarBetween = function(){
            var inicio = $scope.inicio;
            var final = $scope.final;
            $scope.personas = Persona.between({inicio:inicio,final:final});
            $scope.personas.$promise.then(function(result){
                $scope.personas = result;
            })
        }
        $scope.loadData = function(){
            $scope.personas = Persona.getAll();
            $scope.personas.$promise.then(function (result) {
                $scope.personas = result;

            });
        }
        $scope.createAviso = function(persona){
            alert("TODO");
        }
        $scope.createTarea = function(persona){
            alert("TODO");
        }

        $scope.marcar = function(item){//Se le pasa el objeto en lugar del indice debido a la ordenacion que lo deja mal
            if( item.Marcada == true){
                item.Marcada = false;
                var index = $scope.personas.indexOf(item);
                $scope.personas[index]= item;
                Persona.marcar({id:item.Id});

            }else {
                item.Marcada =  true;
                var index = $scope.personas.indexOf(item);
                $scope.personas[index]= item;
                Persona.marcar({id: item.Id});
            }
        }

        $scope.orderFields = [
            "Texto",
            "Comunidad",
            "Fecha"

        ]


        $scope.tooltips = {
            list: {
                "title": "Movimientos de la persona",
                "placement": 'right',
                delay: 500
            },
            aviso: {
                "title": "Crear Aviso a partir de esta persona",
                delay: 500
            },
            editar: {
                "title": "Editar persona",
                delay: 500
            },
            borrar: {
                "title": "Borrar"
            }
        };
    }]);
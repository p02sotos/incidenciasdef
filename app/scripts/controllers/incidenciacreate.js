'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:IncidenciacreateCtrl
 * @description
 * # IncidenciacreateCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('IncidenciacreateCtrl',['$scope','$filter', '$location','Incidencia','Telefono', 'Persona','Comunidad','$alert', function ($scope,$filter,$location, Incidencia,Telefono, Persona,Comunidad,$alert) {
        var actualDate = new Date();
        $scope.incidencia = {
            ComunidadNombre: '',
            Breve:'',
            PersonaNombre:'',
            PersonaId:'',
            Expediente:'',
            Origen:'',
            Prioridad:'3',
            Atencion:'',
            FechaIncidencia: $filter('date')(actualDate, "yyyy-MM-dd"),
            ComunidadId:0,
            Descripcion:"",
            Telefonos: []
        };
        var alert3 = $alert({title: 'Alerta!', content: "No has seleccionado una comunidad en la tabla de la Derecha", duration:2, placement: 'top', type: 'info', keyboard: true, show: false, container:"#alertComunidad"});
        $scope.submit = function(){
            if($scope.incidencia.ComunidadId==0){
                alert3.show();
            }else {
            	$scope.incidencia.Prioridad = $scope.selectedOption;
                $scope.incidencia = Incidencia.create($scope.incidencia);
                $scope.incidencia.$promise.then(function(){
                    $location.path('/incidencia');
                });
            }

        };
        $scope.prioridadClass = function(selectedOption){
            return {
                'label-success': selectedOption==1,
                'label-primary': selectedOption==2,
                'label-info': selectedOption==3,
                'label-danger': selectedOption==5,
                'label-warning': selectedOption==4
            }
        }
        $scope.pri = '3';
        $scope.prioridadValues = [{'value': '1', 'label':'Poca'}, {'value': '2', 'label':'Algo'},{'value': '3', 'label':'Normal'},{'value': '4', 'label':'Importante'},{'value': '5', 'label':'Muy Importante'}];


        $scope.comunidades = Comunidad.getAll();
        $scope.comunidades.$promise.then(function(result){
            $scope.comunidades = result;
        });
        $scope.selectComunidad = function(nombre, id){
            $scope.comunidadNombre = nombre;
            $scope.incidencia.ComunidadId = id;
        };
        var alert1 = $alert({title: 'Alerta!', content: "Número repetido", duration:2, placement: 'top', type: 'info', keyboard: true, show: false, container:"#alert"});
        var alert2 = $alert({title: 'Alerta!', content: "No has introducido nada", duration:2, placement: 'top', type: 'info', keyboard: true, show: false, container:"#alert"});
        $scope.isPhoneRepeated = false;
        $scope.telefonos = [];
        $scope.addPhone = function(){
            $scope.isPhoneRepeated = false;
            if(_.contains($scope.telefonos,$scope.telefono)){
                alert1.show();

            }else if (_.isEmpty($scope.telefono)){

                alert2.show();

            }
            else {
                $scope.telefonos.push($scope.telefono);
                $scope.incidencia.Telefonos = $scope.telefonos;

            }
            $scope.telefono ="";


        }
        $scope.deletePhone = function(){
            $scope.telefonos.pop();
            $scope.incidencia.Telefonos = $scope.telefonos;

        }

        $scope.selectPersona= false;
        $scope.selectComunidad= false;
        $scope.selectPersonaId= function(persona){

            $scope.incidencia.PersonaNombre = persona.Nombre;
            $scope.incidencia.PersonaId = persona.Id;
        };
        $scope.selectComunidadId= function(comunidad){
            $scope.comunidadNombre = comunidad.Nombre;
            $scope.incidencia.ComunidadId = comunidad.Id;

        };

        $scope.showAsociation = function(association){
            if(association=="persona"){
                $scope.selectPersona= true;
                $scope.selectComunidad= false;
                $scope.personas = Persona.getAll();
                $scope.personas.$promise.then(function(result){
                    $scope.personas = result;
                });
            }else if(association=="comunidad") {
                $scope.selectPersona= false;
                $scope.selectComunidad= true;
                $scope.comunidades = Comunidad.getAll();
                $scope.comunidades.$promise.then(function(result){
                    $scope.comunidades = result;
                });
            }
        }

    }]);
'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:ComunidadcreatecrtlCtrl
 * @description
 * # ComunidadcreatecrtlCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('ComunidadCreateCtrl',['$scope', '$location','Comunidad' , function ($scope,$location, Comunidad) {
        $scope.comunidad = {
            Nombre: '',
            Direccion:'',
            Cif: '',
            Cuenta:'',
            Presidente:''
        };
        $scope.submit = function(){
            $scope.comunidad = Comunidad.create($scope.comunidad);
            $scope.comunidad.$promise.then(function(){
                $location.path('/comunidad');
            });
        };
  }]);

'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:PersonacreateCtrl
 * @description
 * # PersonacreateCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('PersonacreateCtrl',['$scope', '$location','Persona','Telefono', 'Comunidad','$alert', function ($scope,$location, Persona,Telefono, Comunidad,$alert) {
        var actualDate = new Date();
        $scope.persona = {
            ComunidadNombre: '',
            Nota:'',
            FechaEntrega: actualDate,
            ComunidadId:0,
            Direccion:"",
            Telefonos: []
        };
        var alert3 = $alert({title: 'Alerta!', content: "No has seleccionado una comunidad en la tabla de la Derecha", duration:2, placement: 'top', type: 'info', keyboard: true, show: false, container:"#alertComunidad"});
        $scope.submit = function(){
            if($scope.persona.ComunidadId==0){
                alert3.show();
            }else {
                $scope.persona = Persona.create($scope.persona);
                $scope.persona.$promise.then(function(){
                    $location.path('/persona');
                });
            }

        };


        $scope.comunidades = Comunidad.getAll();
        $scope.comunidades.$promise.then(function(result){
            $scope.comunidades = result;
        });
        $scope.selectComunidad = function(nombre, id){
            $scope.comunidadNombre = nombre;
            $scope.persona.ComunidadId = id;
        };
        var alert1 = $alert({title: 'Alerta!', content: "Número repetido", duration:2, placement: 'top', type: 'info', keyboard: true, show: false, container:"#alert"});
        var alert2 = $alert({title: 'Alerta!', content: "No has introducido nada", duration:2, placement: 'top', type: 'info', keyboard: true, show: false, container:"#alert"});
        $scope.isPhoneRepeated = false;
        $scope.telefonos = [];
        $scope.addPhone = function(){
            $scope.isPhoneRepeated = false;
            if(_.contains($scope.telefonos,$scope.telefono)){
                alert1.show();

            }else if (_.isEmpty($scope.telefono)){

                alert2.show();

            }
            else {
                $scope.telefonos.push($scope.telefono);
                $scope.persona.Telefonos = $scope.telefonos;

            }
            $scope.telefono ="";


        }
        $scope.deletePhone = function(){
            $scope.telefonos.pop();
            $scope.persona.Telefonos = $scope.telefonos;

        }


    }]);

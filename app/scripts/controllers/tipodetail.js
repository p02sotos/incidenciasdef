'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:TipodetailCtrl
 * @description
 * # TipodetailCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('TipodetailCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

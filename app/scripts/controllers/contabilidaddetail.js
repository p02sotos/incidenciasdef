'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:ContabilidaddetailCtrl
 * @description
 * # ContabilidaddetailCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('ContabilidaddetailCtrl',['$scope', '$location','$routeParams','Contabilidad','Comunidad', function ($scope,$location,$routeParams,Contabilidad, Comunidad) {

        $scope.contabilidad = Contabilidad.get({id:$routeParams.id});
        $scope.contabilidad.$promise.then(function(result){
            $scope.contabilidad = result;
            $scope.comunidadNombre = $scope.contabilidad['Comunidad.Nombre'];
        });


        $scope.submit= function(){

            $scope.cont = Contabilidad.update({id:$routeParams.id},$scope.contabilidad);
            $scope.cont.$promise.then(function(){
                $location.path('/contabilidad');
            });

        };
        $scope.comunidades = Comunidad.getAll();

        $scope.comunidades.$promise.then(function(result){
            $scope.comunidades = result;

        });

        $scope.selectComunidad = function(nombre, id){
            $scope.comunidadNombre = nombre;
            $scope.contabilidad['Comunidad.Id'] = id;

        };


    }]);

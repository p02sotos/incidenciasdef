'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:TelefonocreateCtrl
 * @description
 * # TelefonocreateCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('TelefonocreateCtrl',['$scope', '$location','Telefono','Empresa','Incidencia','Persona', function ($scope,$location, Telefono, Empresa,Incidencia,Persona) {
        var actualDate = new Date();
        $scope.selectPersona= false;
        $scope.selectEmpresa= false;
        $scope.selectIncidencia= false;
        $scope.telefono = {
            Telefono: '',
            Nota:'',
            IncidenciaId: '',
            EmpresaId:'',
            PersonaId:'',
            Tipo:''


        };
        $scope.submit = function(){
            var invalidPhone = false;
            if($scope.telefono.EmpresaId!==null){
                var phone = Telefono.checkPhoneEmpresa({phone:$scope.telefono.Telefono,id:$scope.telefono.EmpresaId});
                phone.$promise.then(function(result){
                   if (result.Error =="ERROR"){
                       invalidPhone = true;
                       invalid();


                   }else{
                       validPhone();
                   }
                });
            }else if ( $scope.telefono.IncidenciaId!==null){
                var phone = Telefono.checkPhoneIncidencia({phone:$scope.telefono.Telefono,id:$scope.telefono.IncidenciaId});
                phone.$promise.then(function(result){
                    if (result.Error =="ERROR"){
                        invalid();

                    }else{
                        validPhone();
                    }
                });

            }else if($scope.telefono.PersonaId!==null){
                var phone = Telefono.checkPhonePersona({phone:$scope.telefono.Telefono,id:$scope.telefono.PersonaId});
                phone.$promise.then(function(result){
                    if (result.Error =="ERROR"){
                        invalidPhone = true;
                        invalid();


                    }else{
                        validPhone();
                    }
                });
            }

             function validPhone(){
                if(!invalidPhone){
                    $scope.telefono = Telefono.create($scope.telefono);
                    $scope.telefono.$promise.then(function(){
                        $location.path('/telefono');
                    });
                }
            }
            function invalid(){
                alert("invalido");
            }
        };

        $scope.selectPersonaId= function(persona){

            $scope.asociacion = persona.Nombre;
            $scope.telefono.EmpresaId =null;
            $scope.telefono.IncidenciaId = null;
            $scope.telefono.PersonaId = persona.Id;
        }
        $scope.selectEmpresaId= function(empresa){

            $scope.asociacion = empresa.Nombre;
            $scope.telefono.EmpresaId = empresa.Id;
            $scope.telefono.PersonaId =null;
            $scope.telefono.IncidenciaId = null;
        }
        $scope.selectIncidenciaId= function(incidencia){

            $scope.asociacion = incidencia.Breve;
            $scope.telefono.IncidenciaId = incidencia.Id;
            $scope.telefono.EmpresaId =null;
            $scope.telefono.PersonaId = null;
        }



        $scope.showAsociation = function(association){
            if(association=="persona"){
                $scope.selectPersona= true;
                $scope.selectEmpresa= false;
                $scope.selectIncidencia= false;
                $scope.personas = Persona.getAll();
                $scope.personas.$promise.then(function(result){
                    $scope.personas = result;
                });
            }else if(association=="empresa") {
                $scope.selectPersona= false;
                $scope.selectEmpresa= true;
                $scope.selectIncidencia= false;
                $scope.empresas = Empresa.getAll();
                $scope.empresas.$promise.then(function(result){
                    $scope.empresas = result;
                });
            }else if(association=="incidencia"){
                $scope.selectPersona= false;
                $scope.selectEmpresa= false;
                $scope.selectIncidencia= true;
                $scope.incidencias = Incidencia.getAll(); //TODO
                $scope.incidencias.$promise.then(function(result){
                    $scope.incidencias = result;
                });
            }
        }
    }]);

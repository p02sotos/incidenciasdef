'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:TipoCtrl
 * @description
 * # TipoCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('TipoCtrl',['$scope', '$location' , '$routeParams', 'Tipo' , function ($scope, $location, $routeParams, Tipo) {
        $scope.tipos = Tipo.getAll();
        $scope.tipos.$promise.then(function (result) {
            $scope.tipos = result;
        });
        $scope.deleteId = '';

        $scope.edit = function ($id) {
            $location.path('/tipo/view/' + $id);
        };
        $scope.delete = function ($id) {
            $scope.tipos = Tipo.delete({id: $id});
            $scope.tipos.$promise.then(function () {
                $scope.tipos = Tipo.getAll();
            });
        };
        $scope.create = function () {
            $location.path('/tipo/create');
        };
        $scope.getIdToRemove = function(id){
            console.log('Id a borrar: '+id);
            $scope.deleteId = id;
        };

        $scope.tipo = {
            Tipo: '',
            Notas:''

        };
        $scope.submit = function(){
            $scope.tipo = Tipo.create($scope.tipo);
            $scope.tipo.$promise.then(function(){
                $location.path('/tipo/');
            });
        };

    }]);

'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:TareacreateCtrl
 * @description
 * # TareacreateCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('TareacreateCtrl',['$scope', '$location','Tarea', 'Incidencia', function ($scope,$location, Tarea, Incidencia) {
        var actualDate = new Date();
        $scope.tarea = {
            Nombre: '',
            Descripcion:'',
            FechaTarea: actualDate,
            IncidenciaId:'',
            Orden: ''

        };
        $scope.submit = function(){
            $scope.tarea.Prioridad = $scope.selectedOption;

            $scope.tarea = Tarea.create($scope.tarea);
            $scope.tarea.$promise.then(function(){
                $location.path('/tarea');
            });
        };
        $scope.prioridadClass = function(selectedOption){
            return {
                'label-success': selectedOption==1,
                'label-primary': selectedOption==2,
                'label-info': selectedOption==3,
                'label-danger': selectedOption==5,
                'label-warning': selectedOption==4
            }
        }
        $scope.pri = '3';
        $scope.prioridadValues = [{'value': '1', 'label':'Poca'}, {'value': '2', 'label':'Algo'},{'value': '3', 'label':'Normal'},{'value': '4', 'label':'Importante'},{'value': '5', 'label':'Muy Importante'}];


        $scope.incidencias = Incidencia.getAll();
        $scope.incidencias.$promise.then(function(result){
            $scope.incidencias = result;
        });
        $scope.selectIncidencia = function(nombre, id){
            $scope.tarea.IncidenciaId = id;
        };



    }]);

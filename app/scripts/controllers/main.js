'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name incidenciasDefApp.controller:TelefonodetailCtrl
 * @description
 * # TelefonodetailCtrl
 * Controller of the incidenciasDefApp
 */
angular.module('incidenciasDefApp')
  .controller('TelefonodetailCtrl',['$scope', '$location','$routeParams','Telefono','Empresa','Incidencia','Persona', function ($scope,$location, $routeParams, Telefono, Empresa,Incidencia,Persona) {
        var actualDate = new Date();
        $scope.selectPersona= false;
        $scope.selectEmpresa= false;
        $scope.selectIncidencia= false;
        $scope.telefono = Telefono.get({id:$routeParams.id});
        $scope.telefono.$promise.then(function(result){
            $scope.telefono = result;
            $scope.asoc;
            if ($scope.telefono.EmpresaId!==null){
                $scope.button={
                    'radio': 1
                };
                if ($scope.telefono.EmpresaId!==0){
                    $scope.asoc =Empresa.get({id:$scope.telefono.EmpresaId});

                    $scope.asoc.$promise.then(function(){
                        $scope.asociacion = $scope.asoc.Nombre;
                    });
                }



            }else if ($scope.telefono.IncidenciaId!==null){
                $scope.button={
                    'radio': 2
                };
                if ($scope.telefono.IncidenciaId!==0){
                    $scope.asoc = Incidencia.get({id:$scope.telefono.IncidenciaId});
                    $scope.asoc.$promise.then(function(){
                        $scope.asociacion = $scope.asoc.Resumen;
                    });
                }



            }else if($scope.telefono.PersonaId!==null) {
                $scope.button={
                    'radio': 0
                };
                if ($scope.telefono.PersonaId!==0){
                    $scope.asoc = Persona.getID({id:$scope.telefono.PersonaId});
                    $scope.asoc.$promise.then(function(){
                        $scope.asociacion = $scope.asoc.Nombre;
                    });
                }


            }

        });
        $scope.submit = function(){

            var invalidPhone = false;
            if($scope.telefono.EmpresaId!==null){
                var phone = Telefono.checkPhoneEmpresa({phone:$scope.telefono.Telefono,id:$scope.telefono.EmpresaId});
                phone.$promise.then(function(result){
                    if (result.Error =="ERROR"){
                        invalidPhone = true;
                        invalid();


                    }else{
                        validPhone();
                    }
                });
            }else if ( $scope.telefono.IncidenciaId!==null){
                var phone = Telefono.checkPhoneIncidencia({phone:$scope.telefono.Telefono,id:$scope.telefono.IncidenciaId});
                phone.$promise.then(function(result){
                    if (result.Error =="ERROR"){
                        invalid();

                    }else{
                        validPhone();
                    }
                });

            }else if($scope.telefono.PersonaId!==null){
                var phone = Telefono.checkPhonePersona({phone:$scope.telefono.Telefono,id:$scope.telefono.PersonaId});
                phone.$promise.then(function(result){
                    if (result.Error =="ERROR"){
                        invalidPhone = true;
                        invalid();


                    }else{
                        validPhone();
                    }
                });
            }

            function validPhone(){
                if(!invalidPhone){
                    $scope.telefono = Telefono.update({id:$routeParams.id},$scope.telefono);
                    $scope.telefono.$promise.then(function(){
                        $location.path('/telefono');
                    });
                }
            }
            function invalid(){
                alert("invalido");
            }




        };

        $scope.selectPersonaId= function(persona){

            $scope.asociacion = persona.Nombre;
            $scope.telefono.EmpresaId =null;
            $scope.telefono.IncidenciaId = null;
            $scope.telefono.PersonaId = persona.Id;
        };
        $scope.selectEmpresaId= function(empresa){

            $scope.asociacion = empresa.Nombre;
            $scope.telefono.EmpresaId = empresa.Id;
            $scope.telefono.PersonaId =null;
            $scope.telefono.IncidenciaId = null;
        };
        $scope.selectIncidenciaId= function(incidencia){

            $scope.asociacion = incidencia.Breve;
            $scope.telefono.IncidenciaId = incidencia.Id;
            $scope.telefono.EmpresaId =null;
            $scope.telefono.PersonaId = null;
        };
        $scope.orderFields = [
            "Texto",
            "Comunidad",
            "Fecha"

        ];



        $scope.showAsociation = function(association){
            if(association=="persona"){
                $scope.selectPersona= true;
                $scope.selectEmpresa= false;
                $scope.selectIncidencia= false;
                $scope.personas = Persona.getAll();
                $scope.personas.$promise.then(function(result){
                    $scope.personas = result;
                });
            }else if(association=="empresa") {
                $scope.selectPersona= false;
                $scope.selectEmpresa= true;
                $scope.selectIncidencia= false;
                $scope.empresas = Empresa.getAll();
                $scope.empresas.$promise.then(function(result){
                    $scope.empresas = result;
                });
            }else if(association=="incidencia"){
                $scope.selectPersona= false;
                $scope.selectEmpresa= false;
                $scope.selectIncidencia= true;
                $scope.incidencias = Incidencia.getAll(); //TODO
                $scope.incidencias.$promise.then(function(result){
                    $scope.incidencias = result;
                });
            }
        }
    }]);

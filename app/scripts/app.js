'use strict';

/**
 * @ngdoc overview
 * @name incidenciasDefApp
 * @description
 * # incidenciasDefApp
 *
 * Main module of the application.
 */
var app = angular
    .module('incidenciasDefApp', [
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'mgcrea.ngStrap'



    ]);
app.config(function ($routeProvider, $httpProvider) {
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $routeProvider
        .when('/', {
            templateUrl: 'views/main.html',
            controller: 'MainCtrl'
        })
        .when('/comunidad', {
            templateUrl: 'views/comunidad.html',
            controller: 'ComunidadCtrl'
        })
        .when('/comunidad/view/:id', {
            templateUrl: 'views/comunidaddetail.html',
            controller: 'ComunidadDetailCtrl'
        })
        .when('/comunidad/create', {
            templateUrl: 'views/comunidaddetail.html',
            controller: 'ComunidadCreateCtrl'
        })
        .when('/entrega', {
            templateUrl: 'views/entrega.html',
            controller: 'EntregaCtrl'
        })
        .when('/entrega/view/:id', {
            templateUrl: 'views/entregadetail.html',
            controller: 'EntregaDetailCtrl'
        })
        .when('/entrega/create', {
            templateUrl: 'views/entregadetail.html',
            controller: 'EntregaCreateCtrl'
        })
        .when('/entrega/calendar', {
            templateUrl: 'views/entregacalendar.html',
            controller: 'EntregaCalendarCtrl'
        })
        .when('/contabilidad', {
          templateUrl: 'views/contabilidad.html',
          controller: 'ContabilidadCtrl'
        })
        .when('/contabilidad/create', {
          templateUrl: 'views/contabilidaddetail.html',
          controller: 'ContabilidadcreateCtrl'
        })
        .when('/contabilidad/view/:id', {
            templateUrl: 'views/contabilidaddetail.html',
            controller: 'ContabilidaddetailCtrl'
        })
        .when('/tipo', {
          templateUrl: 'views/tipo.html',
          controller: 'TipoCtrl'
        })
        .when('/tipo/create', {
          templateUrl: 'views/tipodetail.html',
          controller: 'TipocreateCtrl'
        })
        .when('/tipo/view/:id', {
            templateUrl: 'views/tipodetail.html',
            controller: 'TipodetailCtrl'
        })
        .when('/aviso', {
          templateUrl: 'views/aviso.html',
          controller: 'AvisoCtrl'
        })
        .when('/aviso/view/:id', {
            templateUrl: 'views/avisodetail.html',
            controller: 'AvisodetailCtrl'
        })
        .when('/aviso/create', {
            templateUrl: 'views/avisodetail.html',
            controller: 'AvisocreateCtrl'
        })
        .when('/caja', {
          templateUrl: 'views/caja.html',
          controller: 'CajaCtrl'
        })
        .when('/caja/view/:id', {
          templateUrl: 'views/cajadetail.html',
          controller: 'CajadetailCtrl'
        })
        .when('/caja/create', {
            templateUrl: 'views/cajadetail.html',
            controller: 'CajacreateCtrl'
        })
        .when('/persona', {
          templateUrl: 'views/persona.html',
          controller: 'PersonaCtrl'
        })
        .when('/persona/view/:id', {
          templateUrl: 'views/personadetail.html',
          controller: 'PersonadetailCtrl'
        })
        .when('/persona/create', {
            templateUrl: 'views/personadetail.html',
            controller: 'PersonacreateCtrl'
        })
        .when('/telefono', {
          templateUrl: 'views/telefono.html',
          controller: 'TelefonoCtrl'
        })
        .when('/telefono/view/:id', {
            templateUrl: 'views/telefonodetail.html',
            controller: 'TelefonodetailCtrl'
        })
        .when('/telefono/create', {
            templateUrl: 'views/telefonodetail.html',
            controller: 'TelefonocreateCtrl'
        })
        .when('/empresa', {
          templateUrl: 'views/empresa.html',
          controller: 'EmpresaCtrl'
        })
        .when('/empresa/create', {
            templateUrl: 'views/empresadetail.html',
            controller: 'EmpresacreateCtrl'
        })
        .when('/empresa/view/:id', {
          templateUrl: 'views/empresadetail.html',
          controller: 'EmpresadetailCtrl'
        })
        .when('/tarea', {
          templateUrl: 'views/tarea.html',
          controller: 'TareaCtrl'
        })
        .when('/tarea/create', {
          templateUrl: 'views/tareadetail.html',
          controller: 'TareacreateCtrl'
        })
        .when('/tarea/view/:id', {
            templateUrl: 'views/tareadetail.html',
            controller: 'TareadetailCtrl'
        })
        .when('/caja/movimiento/:id', {
          templateUrl: 'views/movimiento.html',
          controller: 'MovimientoCtrl'
        })
        .when('/incidencia', {
          templateUrl: 'views/incidencia.html',
          controller: 'IncidenciaCtrl'
        })
        .when('/incidencia/view/:id', {
          templateUrl: 'views/incidenciadetail.html',
          controller: 'IncidenciadetailCtrl'
        })
        .when('/incidencia/create', {
            templateUrl: 'views/incidenciadetail.html',
            controller: 'IncidenciacreateCtrl'
        })
        .otherwise({
            redirectTo: '/'
        });
});

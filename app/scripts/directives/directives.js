/**
 * Created by Ser on 23/06/2014.
 */

angular.module('incidenciasDefApp').directive('datepick', function() {
    return {
        restrict: 'A',
        require : 'ngModel',
        link : function (scope, element, attrs, ngModelCtrl) {
            $(function(){
                element.datepicker({
                    dateFormat:'yy-mm-dd',
                    onSelect:function (date) {
                        scope.$apply(function () {
                            ngModelCtrl.$setViewValue(date);
                        });
                    }
                });
            });
        }
    }
});


angular.module('incidenciasDefApp').directive('leftMenu', function() {

    return {
        restrict: 'AE',
        templateUrl: 'views/partials/leftMenu.html'
    }
});

//TODO
angular.module('incidenciasDefApp').directive('tableEntity', function ()
{
    return {
        restrict: 'E',//<dir-template></dir-template> hace referencia a un elemento/etiqueta html
        templateUrl: '<div class="dirClass"><h1>Directivas con AngularJS</h1>' +
            '<ul><li ng-repeat="value in values">{{value}}</li></ul></div>',
        link: function (scope,element)
        {
            //element hace referencia al div que contiene la directiva
            //en la función link añadimos algo de css con jQuery
            $(".dirClass").css({'background' : 'orange', 'color' : 'white'});
            //y creamos una variable de alcance con scope que contiene un array
            scope.values = ["simple","directiva","con","clases","en","angularjs"];
        }
    };
});
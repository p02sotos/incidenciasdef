# README #

Gestor de Incidencias con funcionalidades para Gestoría de Comunidades de Propietarios

* Version 1.0 beta

### Tecnologías empleadas ###

# Front-End #
* AngularJs
* Bootstrap 3.0
* JQuery
# Back-End#
* PHP 5
* Slim Frameworks
* Propel ORM
* MySQL